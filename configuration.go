package godatabase

import (
	"context"
	"log"
	"os"

	"github.com/jackc/pgx/v4/pgxpool"

	"github.com/rafaelcn/jeolgyu"
)

var (
	connection *pgxpool.Pool

	logger *jeolgyu.Jeolgyu
)

func init() {
	var err error

	logger, err = jeolgyu.New(jeolgyu.Settings{
		Filepath: "log/",
		Filename: "database",
		SinkType: jeolgyu.SinkBoth,
	})

	if err != nil {
		log.Fatalf("Couldn't initialize database logger. Reason %v", err)
	}
}

// Init ...
func Init() error {
	c, err := pgxpool.Connect(context.Background(), os.Getenv("DATABASE_URL"))
	connection = c

	return err
}
