module gitlab.com/sapienze/experimental/go-database

go 1.14

require (
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgconn v1.6.1
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jackc/pgx/v4 v4.7.1
	github.com/rafaelcn/jeolgyu v0.0.0-20200608173507-8098de9a5b15
)
