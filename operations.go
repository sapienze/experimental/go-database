package godatabase

import (
	"context"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Query ....
func Query(query string, arguments []interface{}) (pgx.Rows, error) {
	conn, err := connection.Acquire(context.Background())

	if err != nil {
		logger.Info("Couldn't get a connection with the database. Reason %v",
			err)
		return pgx.Rows{}, err
	}

	defer conn.Release()

	results, err := conn.Query(context.Background(), query, arguments...)

	if err != nil {
		logger.Info("Couldn't execute query. Reason %v", err)
		return pgx.Rows{}, err
	}

	return results, nil
}

// Execute ....
func Execute(query string, arguments []interface{}) (pgconn.CommandTag, error) {
	var result pgconn.CommandTag

	conn, err := connection.Acquire(context.Background())

	if err != nil {
		logger.Info("Couldn't get a connection with the database. Reason %v",
			err)
		return nil, err
	}

	defer conn.Release()

	result, err = conn.Exec(context.Background(), query, arguments...)

	if err != nil {
		logger.Info("Couldn't execute query. Reason %v", err)
	}

	return result, err
}

// Pool returns the pool of connections
func Pool() *pgxpool.Pool {
	return connection
}
